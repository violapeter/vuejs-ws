<div class="ad-summary">
    <div class="loading hidden">
        <div class="spinner"></div>
        Betöltés...
    </div>

    <img src="<?php echo $ad['photo_url']; ?>" alt="Ingatalan képe">

    <div class="summary">
        <div class="data"><?php echo $ad['price']; ?></div>
        <button id="save">
            <?php echo $ad['is_saved']
                ? '<i class="icon icon-star-filled"></i>'
                : '<i class="icon icon-star"></i>'; ?>
            <span class="button-text">
                <?php echo $ad['is_saved'] ? 'Elmentve' : 'Mentés'; ?>
            </span>
        </button>
    </div>
</div>
