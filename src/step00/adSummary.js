/* global document, $, jQuery */

function formatPrice(number) {
  return Number(Number(number / 1000000).toFixed(2));
}

$(document).ready(function () {
  var button = $('#save');
  var priceEl = $('.data');

  priceEl.text(formatPrice(priceEl.text()));

  button.click(function () {
    $.ajax({
      url: 'path/to/save',
      beforeSend: function () {
        $('.loading').removeClass('hidden');
      },
      success: function () {
        var isSaved = button.find('.icon').hasClass('icon-star-filled');

        button.find('.button-text').text(isSaved ? 'Mentés' : 'Elmentve');
        button.find('.icon').html(isSaved ? '<i class="icon icon-star"></i>' : '<i class="icon icon-star-filled"></i>');
      },
      complete: function () {
        $('.loading').addClass('hidden');
      }
    });
  });
});
