/* global AD_SUMMARY_DEFAULT */

import Vue from 'vue';
import AdSummary from './AdSummary.vue';
import axios from 'axios';

const myAdSummary = new Vue({
  render: h => h(AdSummary),
  propsData: AD_SUMMARY_DEFAULT
}).$mount('#ad-summary');

myAdSummary.$on('save', () => {
  myAdSummary.loading = true;

  axios.get('path/to/save').then(response => {
    myAdSummary.isSaved = response.isSaved;
    myAdSummary.loading = false;
  });
});
