export function formatPrice(number) {
  return Number(Number(number / 1000000).toFixed(2));
}
