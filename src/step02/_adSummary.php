<script>
  var AD_SUMMARY_DEFAULT = {
    photoUrl: '<?php echo $ad['photo_url']; ?>',
    price: <?php echo $ad['price']; ?>,
    isSaved: <?php echo $ad['is_saved']; ?>
  };
</script>

<div id="ad-summary"></div>

<script src="./main.min.js"></script>
