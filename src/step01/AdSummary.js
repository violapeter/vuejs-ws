/* global AD_SUMMARY_DEFAULT */

function formatPrice(number) {
  return Number(Number(number / 1000000).toFixed(2));
}

var AdSummary = function () {
  this.isLoading = false;
  this.render(AD_SUMMARY_DEFAULT);
};

AdSummary.prototype.toggleSave = function () {
  $.ajax({
    url: 'path/to/save',
    beforeSend: function (response) {
      this.isLoading = true;
      this.render(response);
    },
    success: function (response) {
      this.isLoading = false;
      var dataToRender = Object.assign(
        {},
        AD_SUMMARY_DEFAULT,
        { isSaved: response.isSaved }
      );
      this.render(dataToRender);
    }
  });
};

AdSummary.prototype.render = function (props) {
  var isLoading = this.isLoading;
  var photoUrl = props.photoUrl;
  var price = props.price;
  var isSaved = props.isSaved;

  $('#ad-summary').html('<div class="ad-summary">' +
    isLoading
      ?
      '<div class="loading hidden">' +
      '<div class="spinner"></div>' +
      'Betöltés...' +
      '</div>'
      : '' +
      '<img src="' + photoUrl + '" alt="Ingatalan képe">' +
      '<div class="summary">' +
      '<div class="data">' + formatPrice(price) + '</div>' +
      '<button id="save">' +
      isSaved
        ? '<i class="icon icon-star-filled"></i>'
        : '<i class="icon icon-star"></i>' +
        '<span class="button-text">' +
        isSaved ? 'Elmentve' : 'Mentés' +
          '</span>' +
          '</button>' +
          '</div>' +
          '</div>');

  $('#save').on('click', function () {
    this.toggleSave();
  }.bind(this));
};

var sidebarAdSummary = new AdSummary();


