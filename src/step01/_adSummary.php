<script>
var AD_SUMMARY_DEFAULT = {
  photoUrl: '<?php echo $ad['photo_url']; ?>',
  price: <?php echo $ad['price']; ?>,
  isSaved: <?php echo $ad['is_saved']; ?>
};

// var AD_SUMMARY_DEFAULT = <?php echo json_encode($ad); ?>;
</script>

<div id="ad-summary"></div>

<script src="adSummary.js"></script>
